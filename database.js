var dbConnString = 'football';
var dbCollections = ['groups'];

var mongojs = require('mongojs');
var db = mongojs.connect(dbConnString, dbCollections);

module.exports = db;