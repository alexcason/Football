//var db = require('../database.js');

var dbConnString = 'football';
var dbCollections = ['groups'];

var mongojs = require('mongojs');
var db = mongojs.connect(dbConnString, dbCollections);

exports.index = function(req, res) {
  	res.render('index', { title: 'Express' });
};

exports.groups = {};

exports.groups.all = function(req, res) {
	db.groups.find(function(err, groups) {
    	if (err) return;

    	res.json(groups);
  	});
};

exports.groups.one = function(req, res) {
	var groupId = mongojs.connect(dbConnString, dbCollections).ObjectId(req.params.id);

	db.groups.findOne({ '_id': groupId }, function(err, group) {
		if (err) return;

		res.json(group);
	});
};